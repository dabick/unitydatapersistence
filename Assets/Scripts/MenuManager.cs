using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {
    [SerializeField] private Text highScoreText;
    [SerializeField] private Text errorText;
    [SerializeField] private InputField nameInput;

    void Start() {
        var playerName = DataManager.Instance.PlayerName;
        if (!string.IsNullOrEmpty(playerName)) {
            nameInput.text = playerName;
        }

        var highScore = DataManager.Instance.HighScore;
        if (highScore > 0) {
            highScoreText.text = $"Best Score : {DataManager.Instance.HighScoreName} : {highScore}";
        }
    }

    public void StartNew() {
        var playerName = nameInput.text;
        var isValid = !string.IsNullOrEmpty(playerName);
        errorText.gameObject.SetActive(!isValid);
        if (isValid) {
            DataManager.Instance.SetPlayerName(playerName);
            SceneManager.LoadScene(1);
        }
    }

    public void Exit() {
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else
        Application.Quit();
#endif
    }
}

using System;
using System.IO;
using UnityEngine;

public class DataManager : MonoBehaviour {
    public static DataManager Instance;

    public string PlayerName { private set; get; } = "";
    public int HighScore { private set; get; } = 0;
    public string HighScoreName { private set; get; } = "";

    private void Awake() {
        if (Instance != null) {
            Destroy(Instance);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);

        LoadData();
    }

    [Serializable]
    class PersistData {
        public string LastPlayerName;
        public int HighScore;
        public string HighScorePlayerName;
    }

    public void NewScore(int highScore) {
        if (highScore > HighScore) {
            HighScoreName = PlayerName;
            HighScore = highScore;
            SaveData();
        }
    }

    public void SetPlayerName(string playerName) {
        if (playerName != PlayerName) {
            PlayerName = playerName;
            SaveData();
        }
    }

    public void SaveData() {
        var data = new PersistData {
            LastPlayerName = PlayerName,
            HighScore = HighScore,
            HighScorePlayerName = HighScoreName
        };

        var json = JsonUtility.ToJson(data);

        File.WriteAllText(Application.persistentDataPath + "/savefile.json", json);
    }

    public void LoadData() {
        var path = Application.persistentDataPath + "/savefile.json";
        if (File.Exists(path)) {
            var json = File.ReadAllText(path);
            var data = JsonUtility.FromJson<PersistData>(json);
            PlayerName = data.LastPlayerName;
            HighScore = data.HighScore;
            HighScoreName = data.HighScorePlayerName;
        }
    }
}
